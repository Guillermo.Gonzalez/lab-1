package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
        //findLongestWords("Game", "Action", "Champion");
        
        /* boolean leapYear1 = isLeapYear(2022);
        System.out.println(leapYear1); // true

        boolean leapYear2 = isLeapYear(1996);
        System.out.println(leapYear2); // true

        boolean leapYear3 = isLeapYear(1900);
        System.out.println(leapYear3); // false

        boolean leapYear4 = isLeapYear(2000);
        System.out.println(leapYear4); // true */

        boolean evenPositive1 = isEvenPositiveInt(123456);
        System.out.println(evenPositive1); // true

        boolean evenPositive2 = isEvenPositiveInt(-2);
        System.out.println(evenPositive2); // false

        boolean evenPositive3 = isEvenPositiveInt(123);
        System.out.println(evenPositive3); // false
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
        //lager nye feltvariabler

        int length1 = word1.length();
        int length2 = word2.length();
        int length3 = word3.length();

        int maxLength = Math.max(word1.length(),Math.max(word2.length(), word3.length()));
        
        if (length1 == maxLength){
            System.out.println(word1);
        }
        if (length2 == maxLength){
            System.out.println(word2);
        }
        if (length3 == maxLength){
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0){
            return true;
        }
        else if (year % 100 == 0){
            return false;
        }
        else if(year % 4 == 0){
            return true;
        }
        else return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        } else return false;
    }

}
